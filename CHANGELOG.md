# Version 0.0.0
INI : Add gitignore, changelog and readme files.
FEA : Add kafka, mongo and token util modules.
FEA : Add package.json file.
ENH : Add common-config and common-logger as npm dependencies.
ENH : Bundle all utils modules in index.js and set it as main script in package.json.
ENH : Remove index.js bundle.
BUG : Correct a bug with topics config in default consumer.
ENH : Set produce topics to producer config and consume topics to consumer config.
FEA : Add thrift service util.
BUG : Correct a bug with parsing kafka message format.
BUG : Correct a bug with calling Promises.all instead of Promise.all in kafka emit function.
ENH : Fix dependencies versions.
ENH : Replace HighLevelConsumer by simples Consumer.
FEA : Add populate util to populate documents using thrift services.
FEA : Add type option to token module.
ENH : Make Kafka class instantiable and add shared option to set a singleton shared instance.
ENH : Apply eslint recommended syntax.
BUG : Pass a callback function to producer.createTopics to prevent a error with kafka-node.
ENH : Use new getPath function in populate util module to enable more powerful population.
ENH : Apply eslint recommended syntax to populate util module.
ENH : Set retries and setTimeout options of zookeeper client for node-kafka.
ENH : Add consumer logs for error and connected events.
FEA : Add counter module that enables auto-increment.
ENH : Split consumer and producer setup in kafka module.
FIX : Commit changelog modifications.
FEA : Add connectOpts to connect function options.
