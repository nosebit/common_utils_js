let jwt             = require("jsonwebtoken"),
    redis           = require("redis"),
    lodash          = require("lodash"),
    moment          = require("moment"),
    config          = require("common-config"),
    LoggerFactory   = require("common-logger");

let Logger = new LoggerFactory("TokenUtil");

module.exports = class TokenUtil {
    constructor(opts = {}, redisConfig = {}) {
        let logger = Logger.create("constructor");
        logger.info("enter", opts);

        this.opts = lodash.merge({
            type: "token",
            redisIdx: 1,
            secret: "shhhhhhhhhhhhhh",
            expiration: 24*60*60        // 1 day
        }, opts);

        // Create redis client
        let client = this.client = new redis.createClient(
            lodash.merge({}, config.db.redis, opts.redis, redisConfig)
        );

        client.on("connect", () => { logger.info("redis connected"); });
        client.on("error", (error) => { logger.error("redis error", error); });
        client.on("reconnecting", () => { logger.debug("redis reconnecting"); });
    }

    /**
     * This function generates a new valid token.
     *
     * @param {Object} data
     *     Data to be encoded with token.
     * @param {String} trackId
     *     Logger track id
     * @return {String}
     *     The generated token.
     */
    create(data, trackId) {
        let logger = Logger.create("create", trackId);
        logger.info("enter", {data: data});

        return jwt.sign(data, this.opts.secret, {
            expiresIn: this.opts.expiration
        });
    }

    /**
     * This function decodes a token (checking if it"s valid or not).
     *
     * @param {String} token
     *     The token to be decoded.
     * @return {Object}
     *     The decoded token.
     */
    async decode(token, trackId) {
        let logger = Logger.create("decode", trackId);
        logger.info("enter", {token: token});

        return new Promise((resolve, reject) => {
            jwt.verify(token, this.opts.secret, (error, decode) => {
                if(error) {
                    logger.error("jwt verify error", error);
                    return reject(error);
                }

                logger.info("jwt verify success", decode);

                let key = `${this.opts.type}_${token}`;

                // Check if token is not in the black list (manually expired).
                //
                // @NOTE : JsonWebToken sucks here because it"s not possible
                // to manually expire a token. To check if a token is already
                // expired we keep a "black list" in redis.
                this.client.get(key, (error, result) => {
                    if(error) {
                        logger.error("client get error", error);
                        return reject(error);
                    }
                    else if(result) {
                        logger.info("client get success", result);
                        return reject("token in black list");
                    }

                    logger.info("client get success", result);
                    resolve(decode);
                });
            });
        });
    }

    /**
     * This function manually expires a token.
     *
     * @param {String} token
     *     The token to be expired.
     */
    async expire(token, trackId) {
        let logger = Logger.create("expire", trackId);
        logger.info("enter", {
            token: process.env.NODE_ENV!="production"?token:undefined
        });

        return new Promise((resolve, reject) => {
            jwt.verify(token, this.opts.secret, (error, decode) => {
                if(error) {
                    logger.error("jwt verify error", error);

                    // If token is already expired, then return success.
                    if(error.name == "TokenExpiredError") {
                        return resolve();
                    }

                    return reject(error);
                }

                logger.info("jwt verify success", decode);

                let key = `${this.opts.type}_${token}`;

                // Put token in the black list.
                this.client.set(key, moment().toISOString(), (error, result) => {
                    if(error) {
                        logger.error("client set error", error);
                        return reject(error);
                    }

                    // Remove the token from the black list after some time.
                    this.client.expire(token, this.opts.expiration);

                    logger.info("client set success", result);
                    resolve();
                });
            });
        });
    }
};
