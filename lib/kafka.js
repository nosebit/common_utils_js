let kafka           = require("kafka-node"),
    lodash          = require("lodash"),
    moment          = require("moment"),
    LoggerFactory   = require("common-logger");

let Logger = new LoggerFactory("kafka");

module.exports = class Kafka {
    constructor(config = {}) {
        this.config = lodash.merge({
            producerOnly: false,
            consumerOnly: false,
            zkHost: "localhost:2181",
            shared: false,
            producer: {requireAcks: 1},
            consumer: {}
        }, config);

        // Set as shared instance.
        if(config.shared) { Kafka.shared = this; }

        // Setup producer and consumer
        this.setupProducer();
        this.setupConsumer();
    }

    /**
     * This function setup producer.
     */
    setupProducer() {
        if(this.config.consumerOnly) {return;}

        let logger = Logger.create("setupProducer");
        logger.info("enter");

        let producerConfig = lodash.pick(this.config.producer, [
            "requireAcks"
        ]);

        // Instantiate producer.
        this.producer = new kafka.Producer(
            new kafka.Client(this.config.zkHost, "kafka-node-client"),
            producerConfig
        );

        // On producer error close connection and try again later.
        this.producer.on("error", (error) => {
            logger.error("error", error);

            /*this.producer.close();

            setTimeout(() => {
                this.setupProducer();
            }, 5000);*/
        });
    }

    /**
     * This function setup consumer.
     */
    setupConsumer() {
        if(this.config.producerOnly) {return;}

        let logger = Logger.create("setupConsumer");
        logger.info("enter");

        // Setup a consumer group.
        if(lodash.get(this.config,"consumer.groupId")) {
            logger.info("consumer group");

            let consumerGroupConfig = lodash.merge({
                zk: undefined,
                batch: undefined,
                ssl: false,
                sessionTimeout: 15000,
                protocol: ["roundrobin"],
                fromOffset: "latest",
                migrateHLC: false,
                migrateRolling: true
            }, lodash.pick(this.config.consumer, [
                "zk","batch","ssl","sessionTimeout",
                "protocol", "fromOffset", "migrateHLC",
                "migrateRolling", "groupId"
            ]), {
                host: this.config.zkHost
            });

            this.consumer = new kafka.ConsumerGroup(consumerGroupConfig, this.config.consumer.topics);
        }
        // Instantiate a regular consumer
        else {
            let consumerConfig = lodash.merge({
                autoCommit: true,
                fetchMaxWaitMs: 1000,
                fetchMaxBytes: 1024 * 1024
            }, lodash.pick(this.config.consumer, [
                "autoCommit","fetchMaxWaitMs",
                "fetchMaxBytes"
            ]));

            let topics = lodash.map(this.config.consumer.topics||[], (topic) => {
                return {topic: topic};
            });

            this.consumer = new kafka.Consumer(
                new kafka.Client(this.config.zkHost, "kafka-node-client"), 
                topics, 
                consumerConfig
            );
        }

        // On consumer error close connection and try again later.
        this.consumer.on("error", (error) => {
            logger.error("error", error);

            /*this.consumer.close();

            setTimeout(() => {
                this.setupConsumer();
            }, 5000);*/
        });

        // Log when consumer is connected.
        this.consumer.on("connected", () => {
            logger.info("connected");
        });
    }

    /**
     * This function emits an event to kafka (if and only if this helper has a producer)
     *
     * @NOTE : Key must be in the form topic1:topic2:msgId. In this case, data gonna
     * be posted to topic1 and topic2 with msgId as id.
     */
    async emit(key, data) {
        if(!this.producer){
            throw "no producer suplied";
        }

        let topics, id, splits = key.split(":");

        if(splits.length == 1){
            topics = this.config.topics;
            id = splits[0];
        }
        else {
            topics = splits.slice(0, splits.length-1);
            id = splits[splits.length-1];
        }

        let promises = [];
        const timestamp = ""+moment().unix();

        lodash.forEach(topics, (topic) => {
            const message = {
                key: topic+":"+id,
                data: data
            };

            let promise = new Promise((resolve, reject) => {
                this.producer.send([{
                    topic: topic,
                    messages: [new kafka.KeyedMessage(timestamp, JSON.stringify(message))],
                    attributes: 0
                }], (error, result) => {
                    if(error){return reject(error);}
                    resolve(result);
                });
            });

            // Push promise into the promises stack.
            promises.push(promise);
        });

        return Promise.all(promises);
    }

    /**
     * This function calls the provided function when an event with a key arrieves.
     * @param  {[type]} key
     *     The key of the event to listen to.
     * @param  {Function} callback
     *     The function to be called.
     */
    on(key, callback = (()=>{})) {
        if(!this.consumer){
            throw "no consumer suplied";
        }

        this.consumer.on("message", function(message) {
            try {
                message = message.value ?
                    JSON.parse(message.value) :
                    message;
            }
            catch(error){
                console.error("error", error);
            }

            if(message.key == key) {callback(message.data);}
        });
    }
};
