/**
 * This module sets an interface for a thrift service.
 *
 * @todo
 *     Maybe this should be globally available to all NodeJS projects.
 *
 * @module
 *     utils/service
 * @copyright
 *     Bruno Fonseca
 */
let thrift          = require("thrift"),
    LoggerFactory   = require("common-logger");

// Instantiate the logger factory.
let Logger = new LoggerFactory("thrift.service");

/**
 * Service class definition
 */
module.exports = class Srv {
    /**
     * This function initialize a new service interface
     * @param  {object} Spec
     *     The thrift generated specification for the service.
     * @param  {object} Types
     *     The thrift generated types for the service.
     */
    constructor(Spec, Types) {
        this.spec = Spec;
        this.types = Types;
        this.connection = null;
        this.client = null;
    }

    /**
     * This function estabilishes a connection with the user service.
     *
     * @param {string} host
     *     The resolvable host address in which user service is running.
     * @param {integer} port
     *     The port number in which user service is listening.
     */
    connect(host) {
        let logger = Logger.create("connect"),
            split = host.split(":"),
            hostname = split[0],
            port = split.length > 1 ? split[1] : 80;

        // Create the connection
        this.connection = thrift.createConnection(hostname, port, {
            debug: true,
            max_attempts: 100       // max number of retries
        });

        // Create the client with spec
        this.client = thrift.createClient(this.spec, this.connection);

        // On error, log it.
        this.connection.on("error", function(error) {
            logger.error("thrift createConnection error", error);
        });
    }
};
