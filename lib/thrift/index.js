let lodash 		= require("lodash");

module.exports = {
	Srv: require("./service"),

	parse: (data) => {
		data = JSON.parse(JSON.stringify(data));
		return lodash.omitBy(data, lodash.isNull);
	}
};