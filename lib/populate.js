let lodash          = require("lodash"),
    LoggerFactory   = require("common-logger");

let Logger = new LoggerFactory("populate");

/**
 * This function get data in complex paths within
 * an object. By complex we mean paths in the form
 * key_1...key_k[r]...key_n where key_k[r] represent
 * a key which value is an array of objects and we 
 * want to extract data within each object in that array.
 */
let getPath = function(obj, path) {
    let logger = Logger.create("getPath");
    logger.debug("enter", {path});

    if(lodash.isString(path) && !path.length) {
        return {value: obj, path: path};
    }
    else if( !lodash.isString(path)
        || !lodash.isObject(obj)) {
        return {value: undefined};
    }

    //console.log("getPath", JSON.stringify(obj), path);

    let index;
    let splits = path.split(".");
    let nextPath = splits.shift();
    let tailPath = splits.join(splits.length>1?".":"");

    logger.debug("paths", {nextPath, tailPath});

    if( (/^.+\[\d+\]$/).test(nextPath) ) {
        //console.log('pattern [9] found');
        index = parseInt(nextPath.replace(/^(.+)\[(\d+)\]$/, "$2"));
        nextPath = nextPath.replace(/^(.+)\[(\d+)\]$/, "$1");
    }
    else if( (/^.+\[\]$/).test(nextPath) ) {
        //console.log('pattern [] found');
        index = -1;
        nextPath = nextPath.replace(/^(.+)\[\]$/, "$1");
    }

    //console.log("nextPath", nextPath);

    obj = obj[nextPath];
    //index = lodash.isArray(obj)&&lodash.isUndefined(index)?-1:index;

    if(lodash.isInteger(index)) {
        if(!lodash.isArray(obj)) { return undefined; }

        if(index >= 0) { obj = obj[index]; }
        else {
            let res, arr = [], paths = [];

            for(let i = 0; i < obj.length; i++) {
                res = getPath(obj[i], tailPath);

                if(!res) {continue;}

                if(lodash.isArray(res.path)) {
                    for(let j = 0; j < res.path.length; j++) {
                        arr.push(res.value[j]);
                        paths.push(`${nextPath}[${i}].${res.path[j]}`);
                    }
                }
                else {
                    arr.push(res.value);
                    paths.push(`${nextPath}[${i}].${res.path}`);
                }
            }

            return {value: arr, path: paths};
        }
    }

    let res = getPath(obj, tailPath);
    let value = res.value;
    let newPath = res.path;

    logger.debug("res", {value, newPath});

    if(lodash.isArray(newPath)) {
        lodash.map(newPath, function(path) {
            return `${nextPath}.${path}`;
        });
    }
    else {
        newPath = newPath.length>0?`${nextPath}.${newPath}`:`${nextPath}`;
    }

    logger.debug("res 2", {value, newPath});

    return {value: value, path: newPath};
};

module.exports = function(
    objs = [],
    pathsToPopulate = [],
    pathsToSrvMap = {},
    opts = {}
) {
    let logger = Logger.create("populate"),
        promise = Promise.resolve(objs);

    logger.info("enter", {
        opts,
        objsCount: objs.length,
        pathsToPopulate: pathsToPopulate,
        pathsToSrvMap: lodash.keys(pathsToSrvMap)
    });

    // If there are no objects, return right away.
    if(!objs.length) {
        logger.debug("no objects to populate");
        return promise;
    }

    logger.debug("there are objects to populate");

    lodash.forEach(pathsToPopulate, (path) => {
        logger.debug("path to populate", {path: path});

        if(!path || !pathsToSrvMap[path] || !lodash.get(pathsToSrvMap[path], "client.find")) {
            logger.debug("no path or no pathsToSrvMap find function");
            return;
        }

        let Srv = pathsToSrvMap[path];

        promise = promise.then((objs) => {
            logger.debug(`process ${path}`, {objs: objs});

            let idsSet = new Set();

            // Extract all ids from objs path
            lodash.forEach(objs, (obj) => {
                let res = getPath(obj, path);

                logger.debug(`process ${path} : getPath res`, {res});

                lodash.forEach(lodash.flatten([res.value]), (id) => {
                    logger.debug(`process ${path} : extracting ids`, {obj: obj, id: id});
                    idsSet.add(id);
                });
            });

            let ids = [...idsSet];

            logger.debug(`process ${path} : extracted ids`, ids);

            return Srv.client.find(new Srv.types.Query({_id: ids}))
            .then((pathObjs) => {
                logger.debug(`process ${path} : found pathObjs`, pathObjs);

                let idToPathObjMap = {};

                // Create an id to pathObj map.
                lodash.forEach(pathObjs, (pathObj) => {
                    idToPathObjMap[pathObj._id] = lodash.omitBy(pathObj, lodash.isNull);
                });

                // Populate pathObjs in objs path
                lodash.forEach(objs, (obj) => {
                    let res = getPath(obj, path);
                    let idsList = lodash.isArray(res.path)?lodash.flatten([res.value]):[res.value];
                    let pathsList = lodash.flatten([res.path]);

                    logger.debug(`process ${path} : populate obj`, {
                        res: res,
                        idsList: idsList,
                        pathsList: pathsList
                    });

                    lodash.forEach(pathsList, (inPath, idx) => {
                        let idsList2 = lodash.flatten([idsList[idx]]);
                        let pathObjs = [];

                        logger.debug(`process ${path} : populate obj : inner path ${inPath}`, {
                            idsList2: idsList2
                        });

                        lodash.forEach(idsList2, (id) => {
                            let pathObj = idToPathObjMap[id];
                            if(pathObj) { pathObjs.push(pathObj); }
                        });

                        logger.debug(`process ${path} : pathObjs that gonna populate obj`, {pathObjs});

                        lodash.set(obj, inPath, !pathObjs.length?null:(lodash.isArray(idsList[idx])?pathObjs:pathObjs[0]));
                    });

                    logger.debug(`process ${path} : obj populated`, {obj: obj});
                });

                return objs;
            })
            .catch((error) => {
                logger.error("Srv find error", error);
                
                // Could not retrieve path objs
                return objs;
            });
        });
    });

    return promise;
};
