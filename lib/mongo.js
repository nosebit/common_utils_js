let MongoClient         = require("mongodb"),
    LoggerFactory       = require("common-logger");

let Logger = new LoggerFactory("Mongo");

module.exports = class Mongo {
    /**
     * This function returns a valid connected mongo instance.
     *
     * @param  {String} url
     *     The mongodb url path.
     */
    static async connect(url = "mongodb://localhost:27017", opts = {}) {
        let logger = Logger.create("connect");
        logger.debug("enter", {url: url, opts: opts});

        return MongoClient.connect(url, opts.connectOpts).then((db) => {
            logger.info("mongo client connect success");

            if(!Mongo.db || opts.setAsDefault) { Mongo.db = db; }
            return db;
        }).catch((error) => {
            logger.error("mongo client connect error", error);
            throw error;
        });
    }

    /**
     * This function converts a string id to object id.
     *
     * @param  {String} id
     *     Id in string form
     * @return {Object}
     *     Id in mongo object form
     */
    static toObjectID(id) {
        return MongoClient.ObjectID(id);
    }
};
