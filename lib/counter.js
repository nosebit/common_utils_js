let lodash          = require("lodash"),
    LoggerFactory   = require("common-logger"),
    Mongo           = require("./mongo");

let Logger = new LoggerFactory("counter");

module.exports = class Counter {
    /**
     * This static function setups room db collection.
     */
    static async setupCollection(db) {
        // Local variables
        let collection,
            logger = Logger.create("setupCollection");

        logger.info("enter");

        // Try to create the collection
        try {
            collection = await db.createCollection("counters", {
                readPreference: "secondaryPreferred"
            });

            logger.info("collection create success");
        }
        catch(error) {
            logger.error("collection create error", error);
        }
    }

    /**
     * This function constructs a new instance of this service.
     */
    constructor(opts = {}) {
        let db = opts.db || Mongo.db;

        this.mode = opts.mode || "production";
        this.collection = db.collection("counters");

        if(opts.shared) {Counter.shared = this;}
    }

    /**
     * This function select an entry in counters collection.
     */
    async select(name) {
        let logger = Logger.create("select");
        logger.info("enter", {name});

        try {
            let result = await this.collection.insertOne({
                _id: name,
                count: 0
            });

            logger.debug("collection insertOne success", {id: result.insertedId});
        }
        catch(error) {
            logger.error("collection insertOne error", error);
        }

        // Set name as selected.
        this.name = name;
    }

    /**
     * This function generate next count for a specific name.
     */
    async getNextCount() {
        let logger = Logger.create("getNextCount");
        logger.info("enter", {name: this.name});

        if(!this.name) {throw "no counter selected";}

        let result = await this.collection.findOneAndUpdate({
            _id: this.name
        }, {$inc: {count: 1}}, {returnOriginal: false});

        result = JSON.parse(JSON.stringify(result));

        logger.debug("collection findOneAndUpdate success", result);

        return result.value.count;
    }
}